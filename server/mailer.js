

const nodemailer = require('nodemailer')

exports.sendConfirmationEmail = function ({toUser, hash}) {
    return new Promise((res, rej) => {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 3001,
            secure: true,
            auth: {
                user: process.env.GOOGLE_USER,
                pass: process.env.GOOGLE_PASSWORD
            }
        })

        const message = {
            from: process.env.GOOGLE_USER,
            // use this for Production
            to: toUser.email,
            // to: process.env.GOOGLE_USER,
            subject: "Acoustics Lounge Activate Account",
            html: `
                <h3>Hello ${toUser.username}</h3>
                <p>Thank you for registering.</p>
                <p>To finish account registration, please click the <a target="_" href="${process.env.DOMAIN}/api/activate/user/${hash}">activation link</a>.</p>
                <p>Regards,</p>
                <p>-The Dev-</p>
            `
        }

        transporter.sendMail(message, function(err, info) {
            if(err) {
                rej(err)
            } else {
                res(info);
            }
        })
    })
}